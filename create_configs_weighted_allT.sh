#!/bin/bash
# File Name: create_configs_allT.sh
# Author: Ruth Lorenz
# Created: 22/02/2021
# Modified: Wed 24 Mar 2021 04:48:44 PM CET
# Purpose : create all configs for different time periods

###-------------------------------------------------------
varname="tas"
years="2031 2041 2051 2061 2071 2081"
season="DJF"
cmip="cmip6"

#check if first file exists
conf1=configs/config_${varname}_weighting_2021_${season}_${cmip}.ini
if [ ! -f "$conf1" ]; then
  echo "ERROR: first file $conf1 is missing."
  exit
fi

for YR_START in ${years}
do
  echo $YR_START
  YR_END=$(($YR_START + 19))
  echo $YR_END
  conf=configs/config_${varname}_weighting_${YR_START}_${season}_${cmip}.ini
  echo $conf
  cp $conf1 $conf
  sed -i '19 s/2021/'$YR_START'/; 20 s/2040/'$YR_END'/' $conf
done
