# intercomparison_robustness_CMIP

Analysis to compare robustness in projections from different CMIP generations.
Inspired by Knutti and Sedlacek, 2012, Nature Climate Change

Usage:
python environment: py37
python multi_model_robustness.py -f configs/config.ini

The config file is set up similarly to ClimWIP, weights calculated by ClimWIP
can be given as netcdf file.

Data and plots are saved by variable.

The file calc_performance.py calculates performance measures of an ensemble,
e.g. RMSE or Correlation with observational data (make sure you have the used
diagnostics available for the obs data using a config that calculates them
beforehand)

The postproc folder contains ncl scripts which make it possible to use the
saved output data to plot maps very similarl to Knutti and Sedlacek, 2012, NCC
and AR5. The files res_maps.ncl and res_maps_percent.ncl contain the ncl
plotting resources used in plot_cmip5_cmip5.ncl. 
This folder also contains scripts to plot aggregated figures using CMIP3, CMIP5,
e.g. to compare RMSE to R(obustness).