#!/usr/bin/python
'''
File Name : plot_R_vs_warming.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 14-01-2021
Modified: Fri 19 Feb 2021 08:49:51 AM CET
Purpose:


'''
import numpy as np
import xarray as xr
import logging
import os
import matplotlib.pyplot as plt

# INPUT
indir = '/net/tropo/climphys/rlorenz/Data/IntercomparisonRobustness/'
varname = 'pr'
season = 'ANN'
scenarios = ['sresa2', 'rcp85', 'ssp585']
base_start = '1986'
base_end = '2005'
future_start = ['2021', '2031', '2041', '2051', '2061', '2071', '2081']
future_end = ['2040', '2050', '2060', '2070', '2080', '2090', '2100']
cmip3_data = 'processed_CMIP3_data/%s/netcdf' %(varname)
cmip5_data = 'processed_CMIP5_data_selmodels/%s/netcdf' %(varname)
cmip6_data = 'processed_CMIP6_data_selmodels/%s/netcdf' %(varname)
thres = 0.80 #threshold for R

# READ ROBUSTNESS
def file_str(dir, data, start, end, scenario, season):
    new_string = '%s%s/R_%s-%s_%s_%s.nc' %(dir, data, start, end, scenario, season)
    return new_string

f_robustness_cmip3 = []
f_robustness_cmip5 = []
f_robustness_cmip6 = []
for i in range(0, len(future_start)):
    new_str3 = file_str(
        indir, cmip3_data, future_start[i], future_end[i], scenarios[0], season)
    f_robustness_cmip3.append(new_str3)

    new_str5 = file_str(
        indir, cmip5_data, future_start[i], future_end[i], scenarios[1], season)
    f_robustness_cmip5.append(new_str5)

    new_str6 = file_str(
        indir, cmip6_data, future_start[i], future_end[i], scenarios[2], season)
    f_robustness_cmip6.append(new_str6)

R_cmip3 = xr.open_mfdataset(f_robustness_cmip3, combine="nested", concat_dim='time')['R']
R_cmip5 = xr.open_mfdataset(f_robustness_cmip5, combine="nested", concat_dim='time')['R']
R_cmip6 = xr.open_mfdataset(f_robustness_cmip6, combine="nested", concat_dim='time')['R']

lat = xr.open_dataset(f_robustness_cmip6[0])['lat']
lon = xr.open_dataset(f_robustness_cmip6[0])['lon']


# Calculate fraction of gridpoints above R>thres
total_gp = len(lat) * len(lon)

cmip3_above = R_cmip3.where(R_cmip3>thres, drop=True).compute()
cmip5_above = R_cmip5.where(R_cmip5>thres, drop=True).compute()
cmip6_above = R_cmip6.where(R_cmip6>thres, drop=True).compute()

cmip3_gp = list()
cmip5_gp = list()
cmip6_gp = list()

for t in range(0, len(future_start)):
    gp_above3 = total_gp - np.sum(np.isnan(cmip3_above[t,:,:]))
    cmip3_gp.append(gp_above3 / total_gp * 100.)

    gp_above5 = total_gp - np.sum(np.isnan(cmip5_above[t,:,:]))
    cmip5_gp.append(gp_above5 / total_gp * 100.)

    gp_above6 = total_gp - np.sum(np.isnan(cmip6_above[t,:,:]))
    cmip6_gp.append(gp_above6 / total_gp * 100.)

R_cmip3.close()
R_cmip5.close()
R_cmip6.close()

#import ipdb; ipdb.set_trace()
# PLOT R versus RMSE
#title = '%s %s' %(varname, season)
time_str = ['%s-%s' %(future_start[i], future_end[i]) for i in range(0, len(future_start))]
filename = os.path.join(indir, 'Agg_Plots/R_vs_time_{}_{}_{}-{}_selmodels'.format(varname, season, future_start[0], future_end[-1]))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(time_str, cmip3_gp, 'o', color='tab:red', label='CMIP3')
plt.plot(time_str, cmip5_gp, 's', color='tab:purple', label='CMIP5')
plt.plot(time_str, cmip6_gp, '>', color='tab:blue', label='CMIP6')

#ax.set_ylim(50., 100.)
ax.set_xlabel('Time', fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend(frameon=False, loc='upper left')
plt.savefig(filename + '.pdf', dpi=300)

# PLOT R versus global mean warming instead of time_str
# Read mean warming from tas files
cmip3_data = 'processed_CMIP3_data/tas/netcdf'
cmip5_data = 'processed_CMIP5_data_selmodels/tas/netcdf'
cmip6_data = 'processed_CMIP6_data_selmodels/tas/netcdf'

def file_str2(dir, data, start, end, scenario, season):
    new_string = '%s%s/mean_rel_%s-%s_%s_%s.nc' %(dir, data, start, end, scenario, season)
    return new_string
t_cmip3 = list()
t_cmip5 = list()
t_cmip6 = list()

for i in range(0, len(future_start)):
    f_mean_cmip3 = file_str2(
        indir, cmip3_data, future_start[i], future_end[i], scenarios[0], season)
    with xr.open_dataset(f_mean_cmip3) as ds:
        t_cmip3.append(ds['global_mean_change'].data.item())

    f_mean_cmip5 = file_str2(
        indir, cmip5_data, future_start[i], future_end[i], scenarios[1], season)
    with xr.open_dataset(f_mean_cmip5) as ds:
        t_cmip5.append(ds['global_mean_change'].data.item())

    f_mean_cmip6 = file_str2(
        indir, cmip6_data, future_start[i], future_end[i], scenarios[2], season)
    with xr.open_dataset(f_mean_cmip6) as ds:
        t_cmip6.append(ds['global_mean_change'].data.item())

#import ipdb; ipdb.set_trace()

filename2 = os.path.join(indir, 'Agg_Plots/R_vs_warming_{}_{}_{}-{}_selmodels'.format(varname, season, future_start[0], future_end[-1]))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(t_cmip3, cmip3_gp, 'o', color='tab:red', label='CMIP3')
plt.plot(t_cmip5, cmip5_gp, 's', color='tab:purple', label='CMIP5')
plt.plot(t_cmip6, cmip6_gp, '>', color='tab:blue', label='CMIP6')

#ax.set_ylim(50., 100.)
ax.set_xlabel('Global mean warming', fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend(loc='upper left', frameon=False)
plt.savefig(filename2 + '.pdf', dpi=300)
