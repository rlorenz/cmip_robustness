#!/usr/bin/python
'''
File Name : plot_R_vs_warming_weighted.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 14-01-2021
Modified: Thu 11 Mar 2021 08:06:32 PM CET
Purpose:


'''
import numpy as np
import xarray as xr
import logging
import os
import matplotlib.pyplot as plt

from utils.get_area import get_atm_area

# INPUT
indir = '/net/tropo/climphys/rlorenz/Data/IntercomparisonRobustness/'
varname = 'pr'
season = 'DJF'
scenarios = ['sresa2', 'rcp85', 'ssp585']
base_start = '1986'
base_end = '2005'
future_start = ['2021', '2031', '2041', '2051', '2061', '2071', '2081']
future_end = ['2040', '2050', '2060', '2070', '2080', '2090', '2100']
cmip3_data = 'processed_CMIP3_data/%s/netcdf' %(varname)
cmip3w_data = 'weighted_CMIP3_data/%s/netcdf' %(varname)
cmip5_data = 'processed_CMIP5_data/%s/netcdf' %(varname)
cmip5w_data = 'weighted_CMIP5_data/%s/netcdf' %(varname)
cmip6_data = 'processed_CMIP6_data/%s/netcdf' %(varname)
cmip6w_data = 'weighted_CMIP6_data/%s/netcdf' %(varname)
miroc_data = 'processed_CMIP6_data_MIROC6/%s/netcdf' %(varname)
canesm_data = 'processed_CMIP6_data_CanESM5/%s/netcdf' %(varname)
thres = 0.9 #threshold for R

# READ ROBUSTNESS
def file_str(dir, data, start, end, scenario, season):
    new_string = '%s%s/R_%s-%s_%s_%s.nc' %(dir, data, start, end, scenario, season)
    return new_string

f_robustness_cmip3 = []
f_robustness_cmip3w = []
f_robustness_cmip5 = []
f_robustness_cmip5w = []
f_robustness_cmip6 = []
f_robustness_cmip6w = []
f_robustness_miroc = []
f_robustness_canesm = []
for i in range(0, len(future_start)):
    new_str3 = file_str(
        indir, cmip3_data, future_start[i], future_end[i], scenarios[0], season)
    f_robustness_cmip3.append(new_str3)

    new_str3w = file_str(
        indir, cmip3w_data, future_start[i], future_end[i], scenarios[0], season)
    f_robustness_cmip3w.append(new_str3w)

    new_str5 = file_str(
        indir, cmip5_data, future_start[i], future_end[i], scenarios[1], season)
    f_robustness_cmip5.append(new_str5)

    new_str5w = file_str(
        indir, cmip5w_data, future_start[i], future_end[i], scenarios[1], season)
    f_robustness_cmip5w.append(new_str5w)

    new_str6 = file_str(
        indir, cmip6_data, future_start[i], future_end[i], scenarios[2], season)
    f_robustness_cmip6.append(new_str6)

    new_str6w = file_str(
        indir, cmip6w_data, future_start[i], future_end[i], scenarios[2], season)
    f_robustness_cmip6w.append(new_str6w)

    new_strmi = file_str(
        indir, miroc_data, future_start[i], future_end[i], scenarios[2], season)
    f_robustness_miroc.append(new_strmi)

    new_strca = file_str(
        indir, canesm_data, future_start[i], future_end[i], scenarios[2], season)
    f_robustness_canesm.append(new_strca)

R_cmip3 = xr.open_mfdataset(f_robustness_cmip3, combine="nested", concat_dim='time')['R']
R_cmip3w = xr.open_mfdataset(f_robustness_cmip3w, combine="nested", concat_dim='time')['R']
R_cmip5 = xr.open_mfdataset(f_robustness_cmip5, combine="nested", concat_dim='time')['R']
R_cmip5w = xr.open_mfdataset(f_robustness_cmip5w, combine="nested", concat_dim='time')['R']
R_cmip6 = xr.open_mfdataset(f_robustness_cmip6, combine="nested", concat_dim='time')['R']
R_cmip6w = xr.open_mfdataset(f_robustness_cmip6w, combine="nested", concat_dim='time')['R']
R_miroc = xr.open_mfdataset(f_robustness_miroc, combine="nested", concat_dim='time')['R']
R_canesm = xr.open_mfdataset(f_robustness_canesm, combine="nested", concat_dim='time')['R']

lat = xr.open_dataset(f_robustness_cmip6[0])['lat']
lon = xr.open_dataset(f_robustness_cmip6[0])['lon']


# Calculate fraction of gridpoints above R>thres
total_gp = len(lat) * len(lon)

cmip3_above = R_cmip3.where(R_cmip3>thres)
cmip3w_above = R_cmip3w.where(R_cmip3w>thres)
cmip5_above = R_cmip5.where(R_cmip5>thres)
cmip5w_above = R_cmip5w.where(R_cmip5w>thres)
cmip6_above = R_cmip6.where(R_cmip6>thres)
cmip6w_above = R_cmip6w.where(R_cmip6w>thres)
miroc_above = R_miroc.where(R_miroc>thres)
canesm_above = R_canesm.where(R_canesm>thres)

cmip3_gp = list()
cmip3w_gp = list()
cmip5_gp = list()
cmip5w_gp = list()
cmip6_gp = list()
cmip6w_gp = list()
miroc_gp = list()
canesm_gp = list()

for t in range(0, len(future_start)):
    gp_above3 = total_gp - np.sum(np.isnan(cmip3_above[t,:,:]))
    cmip3_gp.append(gp_above3 / total_gp * 100.)

    gp_above3w = total_gp - np.sum(np.isnan(cmip3w_above[t,:,:]))
    cmip3w_gp.append(gp_above3w / total_gp * 100.)

    gp_above5 = total_gp - np.sum(np.isnan(cmip5_above[t,:,:]))
    cmip5_gp.append(gp_above5 / total_gp * 100.)

    gp_above5w = total_gp - np.sum(np.isnan(cmip5w_above[t,:,:]))
    cmip5w_gp.append(gp_above5w / total_gp * 100.)

    gp_above6 = total_gp - np.sum(np.isnan(cmip6_above[t,:,:]))
    cmip6_gp.append(gp_above6 / total_gp * 100.)

    gp_above6w = total_gp - np.sum(np.isnan(cmip6w_above[t,:,:]))
    cmip6w_gp.append(gp_above6w / total_gp * 100.)

    gp_abovemiroc = total_gp - np.sum(np.isnan(miroc_above[t,:,:]))
    miroc_gp.append(gp_abovemiroc / total_gp * 100.)

    gp_abovecanesm = total_gp - np.sum(np.isnan(canesm_above[t,:,:]))
    canesm_gp.append(gp_abovecanesm / total_gp * 100.)

# Calculate fraction of Earth surface where R>thres
total_area = get_atm_area(lat.data, lon.data)

cmip3_area = list()
cmip3w_area = list()
cmip5_area = list()
cmip5w_area = list()
cmip6_area = list()
cmip6w_area = list()
miroc_area = list()
canesm_area = list()
for t in range(0, len(future_start)):
    area_above3 = np.where(np.isnan(cmip3_above[t,:,:]), np.nan, total_area)
    cmip3_area.append(np.nansum(area_above3) / np.sum(total_area) * 100.)

    area_above3w = np.where(np.isnan(cmip3w_above[t,:,:]), np.nan, total_area)
    cmip3w_area.append(np.nansum(area_above3w) / np.sum(total_area) * 100.)

    area_above5 = np.where(np.isnan(cmip5_above[t,:,:]), np.nan, total_area)
    cmip5_area.append(np.nansum(area_above5) / np.sum(total_area) * 100.)

    area_above5w = np.where(np.isnan(cmip5w_above[t,:,:]), np.nan, total_area)
    cmip5w_area.append(np.nansum(area_above5w) / np.sum(total_area) * 100.)

    area_above6 = np.where(np.isnan(cmip6_above[t,:,:]), np.nan, total_area)
    cmip6_area.append(np.nansum(area_above6) / np.sum(total_area) * 100.)

    area_above6w = np.where(np.isnan(cmip6w_above[t,:,:]), np.nan, total_area)
    cmip6w_area.append(np.nansum(area_above6w) / np.sum(total_area) * 100.)

    area_abovemiroc = np.where(np.isnan(miroc_above[t,:,:]), np.nan, total_area)
    miroc_area.append(np.nansum(area_abovemiroc) / np.sum(total_area) * 100.)

    area_abovecanesm = np.where(np.isnan(canesm_above[t,:,:]), np.nan, total_area)
    canesm_area.append(np.nansum(area_abovecanesm) / np.sum(total_area) * 100.)

R_cmip3.close()
R_cmip3w.close()
R_cmip5.close()
R_cmip5w.close()
R_cmip6.close()
R_cmip6w.close()
R_miroc.close()
R_canesm.close()

#import ipdb; ipdb.set_trace()
# PLOT R versus RMSE
#title = '%s %s' %(varname, season)
time_str = ['%s-%s' %(future_start[i], future_end[i]) for i in range(0, len(future_start))]
filename = os.path.join(indir, 'Agg_Plots/R_weighted356_vs_time_{}_{}_{}_{}-{}'.format(varname, season, thres, future_start[0], future_end[-1]))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(time_str, cmip3_gp, 'o', color='tab:orange', label='CMIP3')
plt.plot(time_str, cmip3w_gp, 'h', color='tab:red', label='CMIP3')
plt.plot(time_str, cmip5_gp, 's', color='tab:purple', label='CMIP5')
plt.plot(time_str, cmip5w_gp, 'D', color='tab:pink', label='CMIP5 weighted')
plt.plot(time_str, cmip6_gp, '>', color='tab:blue', label='CMIP6')
plt.plot(time_str, cmip6w_gp, '<', color='tab:cyan', label='CMIP6 weighted')
plt.plot(time_str, miroc_gp, 'X', color='tab:gray', label='MIROC6 (50)')
plt.plot(time_str, canesm_gp, 'x', color='tab:gray', label='CanESM5 (50)')

ax.set_ylim(0., 100.)
ax.set_xlim(0., 5.)
ax.set_xlabel('Time', fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend(frameon=False, loc='upper left')
plt.savefig(filename + '.pdf', dpi=300)

# PLOT R versus global mean warming instead of time_str
# Read mean warming from tas files
cmip3_data = 'processed_CMIP3_data/tas/netcdf'
cmip3w_data = 'weighted_CMIP3_data/tas/netcdf'
cmip5_data = 'processed_CMIP5_data/tas/netcdf'
cmip5w_data = 'weighted_CMIP5_data/tas/netcdf'
cmip6_data = 'processed_CMIP6_data/tas/netcdf'
cmip6w_data = 'weighted_CMIP6_data/tas/netcdf'
miroc_data = 'processed_CMIP6_data_MIROC6/tas/netcdf'
canesm_data = 'processed_CMIP6_data_CanESM5/tas/netcdf'
def file_str2(dir, data, start, end, scenario, season):
    new_string = '%s%s/mean_rel_%s-%s_%s_%s.nc' %(dir, data, start, end, scenario, season)
    return new_string
t_cmip3 = list()
t_cmip3w = list()
t_cmip5 = list()
t_cmip5w = list()
t_cmip6 = list()
t_cmip6w = list()
t_miroc = list()
t_canesm = list()
for i in range(0, len(future_start)):
    f_mean_cmip3 = file_str2(
        indir, cmip3_data, future_start[i], future_end[i], scenarios[0], season)
    with xr.open_dataset(f_mean_cmip3) as ds:
        t_cmip3.append(ds['global_mean_change'].data.item())

    f_mean_cmip3w = file_str2(
        indir, cmip3w_data, future_start[i], future_end[i], scenarios[0], season)
    with xr.open_dataset(f_mean_cmip3w) as ds:
        t_cmip3w.append(ds['global_mean_change'].data.item())

    f_mean_cmip5 = file_str2(
        indir, cmip5_data, future_start[i], future_end[i], scenarios[1], season)
    with xr.open_dataset(f_mean_cmip5) as ds:
        t_cmip5.append(ds['global_mean_change'].data.item())

    f_mean_cmip5w = file_str2(
        indir, cmip5w_data, future_start[i], future_end[i], scenarios[1], season)
    with xr.open_dataset(f_mean_cmip5w) as ds:
        t_cmip5w.append(ds['global_mean_change'].data.item())

    f_mean_cmip6 = file_str2(
        indir, cmip6_data, future_start[i], future_end[i], scenarios[2], season)
    with xr.open_dataset(f_mean_cmip6) as ds:
        t_cmip6.append(ds['global_mean_change'].data.item())

    f_mean_cmip6w = file_str2(
        indir, cmip6w_data, future_start[i], future_end[i], scenarios[2], season)
    with xr.open_dataset(f_mean_cmip6w) as ds:
        t_cmip6w.append(ds['global_mean_change'].data.item())

    f_mean_miroc = file_str2(
        indir, miroc_data, future_start[i], future_end[i], scenarios[2], season)
    with xr.open_dataset(f_mean_miroc) as ds:
        t_miroc.append(ds['global_mean_change'].data.item())

    f_mean_canesm = file_str2(
        indir, canesm_data, future_start[i], future_end[i], scenarios[2], season)
    with xr.open_dataset(f_mean_canesm) as ds:
        t_canesm.append(ds['global_mean_change'].data.item())

#import ipdb; ipdb.set_trace()

filename2 = os.path.join(indir, 'Agg_Plots/R_gp_weighted356_vs_warming_{}_{}_{}_{}-{}'.format(varname, season, thres, future_start[0], future_end[-1]))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(t_cmip3, cmip3_gp, 'o', color='tab:orange', label='CMIP3')
plt.plot(t_cmip3w, cmip3w_gp, 'h', color='tab:red', label='CMIP3 weighted')
plt.plot(t_cmip5, cmip5_gp, 's', color='tab:purple', label='CMIP5')
plt.plot(t_cmip5w, cmip6w_gp, 'D', color='tab:pink', label='CMIP5 weighted')
plt.plot(t_cmip6, cmip6_gp, '>', color='tab:blue', label='CMIP6')
plt.plot(t_cmip6w, cmip6w_gp, '<', color='tab:cyan', label='CMIP6 weighted')
plt.plot(t_miroc, miroc_gp, 'X', color='tab:gray', label='MIROC6 (50)')
plt.plot(t_canesm, canesm_gp, 'x', color='tab:gray', label='CanESM5 (50)')

ax.set_ylim(0., 100.)
ax.set_xlim(0., 5.)
ax.set_xlabel('Global mean warming', fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend(loc='upper left', frameon=False)
plt.savefig(filename2 + '.pdf', dpi=300)

filename3 = os.path.join(indir, 'Agg_Plots/R_area_weighted356_vs_warming_{}_{}_{}_{}-{}'.format(varname, season, thres, future_start[0], future_end[-1]))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(t_cmip3, cmip3_area, 'o', color='tab:orange', label='CMIP3')
plt.plot(t_cmip3w, cmip3w_area, 'h', color='tab:red', label='CMIP3 weighted')
plt.plot(t_cmip5, cmip5_area, 's', color='tab:purple', label='CMIP5')
plt.plot(t_cmip5w, cmip5w_area, 'D', color='tab:pink', label='CMIP5 weighted')
plt.plot(t_cmip6, cmip6_area, '>', color='tab:blue', label='CMIP6')
plt.plot(t_cmip6w, cmip6w_area, '<', color='tab:cyan', label='CMIP6 weighted')
plt.plot(t_miroc, miroc_area, 'X', color='tab:gray', label='MIROC6 (50)')
plt.plot(t_canesm, canesm_area, 'x', color='tab:gray', label='CanESM5 (50)')

ax.set_ylim(0., 100.)
ax.set_xlim(0., 5.)
ax.set_xlabel('Global mean warming', fontsize='large')
ax.set_ylabel("%% of Earth's surface with R>%s" %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend(loc='upper left', frameon=False)
plt.savefig(filename3 + '.pdf', dpi=300)
