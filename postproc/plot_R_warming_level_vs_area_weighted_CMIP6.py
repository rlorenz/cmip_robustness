#!/usr/bin/python
'''
File Name : plot_R_PDF_weighted.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 17-03-2021
Modified: Wed 17 Mar 2021 03:53:45 PM CET
Purpose: plot distribution of R weighted versus unweighted so see change


'''
import numpy as np
import xarray as xr
import logging
import os
import matplotlib.pyplot as plt

from utils.get_area import get_atm_area

# INPUT
indir = '/net/tropo/climphys/rlorenz/Data/IntercomparisonRobustness/'
varname = 'tas'
season = 'DJF'
scenarios = ['ssp585']
warming_level = 10

cmip6_data = 'processed_CMIP6_data_warm%s/%s/netcdf' %(warming_level, varname)
cmip6w_data = 'weighted_CMIP6_data_warm%s/%s/netcdf' %(warming_level, varname)

# READ ROBUSTNESS
def file_str(dir, data, warm, scenario, season):
    new_string = '%s%s/R_warm%s_%s_%s.nc' %(dir, data, warm, scenario, season)
    return new_string

f_robustness_cmip6 = []
f_robustness_cmip6w = []

f_robustness_cmip6 = file_str(
    indir, cmip6_data, warming_level, scenarios[0], season)

f_robustness_cmip6w = file_str(
    indir, cmip6w_data, warming_level, scenarios[0], season)

R_cmip6 = xr.open_dataset(f_robustness_cmip6)['R']
R_cmip6w = xr.open_dataset(f_robustness_cmip6w)['R']

lat = xr.open_dataset(f_robustness_cmip6)['lat']
lon = xr.open_dataset(f_robustness_cmip6)['lon']

# Calculate fraction of Earth surface
total_area = get_atm_area(lat.data, lon.data)

# Calculate area for R bins
bins = np.arange(0., 1.1, 0.1)
cmip6_area = list()
cmip6w_area = list()
cmip6_area_cdf = list()
cmip6w_area_cdf = list()

for count, value in enumerate(bins):
    if count == 0:
        cmip6_below = R_cmip6.where(R_cmip6<=value)
        area_below6 = np.where(np.isnan(cmip6_below), np.nan, total_area)
        cmip6_area.append(np.nansum(area_below6) / np.sum(total_area) * 100.)

        cmip6_area_cdf.append(np.nansum(area_below6) / np.sum(total_area) * 100.)


        cmip6w_below = R_cmip6w.where(R_cmip6w<=value)
        area_below6w = np.where(np.isnan(cmip6w_below), np.nan, total_area)
        cmip6w_area.append(np.nansum(area_below6w) / np.sum(total_area) * 100.)

        cmip6w_area_cdf.append(np.nansum(area_below6w) / np.sum(total_area) * 100.)

    else:
        cmip6_above = R_cmip6.where(((R_cmip6>bins[count-1]) & (R_cmip6<=value)))
        area_above6 = np.where(np.isnan(cmip6_above), np.nan, total_area)
        cmip6_area.append(np.nansum(area_above6) / np.sum(total_area) * 100.)

        cmip6w_above = R_cmip6w.where(((R_cmip6w>bins[count-1]) & (R_cmip6w<=value)))
        area_above6w = np.where(np.isnan(cmip6w_above), np.nan, total_area)
        cmip6w_area.append(np.nansum(area_above6w) / np.sum(total_area) * 100.)

        # "CDF"
        cmip6_below_value = R_cmip6.where(R_cmip6<=value)
        area_below6 = np.where(np.isnan(cmip6_below_value), np.nan, total_area)
        cmip6_area_cdf.append(np.nansum(area_below6) / np.sum(total_area) * 100.)

        cmip6w_below_value = R_cmip6w.where(R_cmip6w<=value)
        area_below6w = np.where(np.isnan(cmip6w_below_value), np.nan, total_area)
        cmip6w_area_cdf.append(np.nansum(area_below6w) / np.sum(total_area) * 100.)


R_cmip6.close()
R_cmip6w.close()

## Plotting

filename = os.path.join(indir, 'Agg_Plots/warm{}_R_vs_area_weighted6_{}_{}'.format(warming_level, varname, season))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(bins, cmip6_area, '>', color='tab:blue', label='CMIP6')
plt.plot(bins, cmip6w_area, '<', color='tab:cyan', label='CMIP6 weighted')

#ax.set_ylim(0., 100.)
#ax.set_xlim(0., 1.)
ax.set_xlabel('R', fontsize='large')
ax.set_ylabel("% of Earth's surface", fontsize='large')
#ax.set_title(title)
ax.legend(loc='upper left', frameon=False)
plt.savefig(filename + '.pdf', dpi=300)

# CDF
filename2 = os.path.join(indir, 'Agg_Plots/warm{}_R_cdf_vs_area_weighted6_{}_{}'.format(warming_level, varname, season))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(bins, cmip6_area_cdf, '>', color='tab:blue', label='CMIP6')
plt.plot(bins, cmip6w_area_cdf, '<', color='tab:cyan', label='CMIP6 weighted')

#ax.set_ylim(0., 100.)
#ax.set_xlim(0., 1.)
ax.set_xlabel('R', fontsize='large')
ax.set_ylabel("% of Earth's surface", fontsize='large')
#ax.set_title(title)
ax.legend(loc='upper left', frameon=False)
plt.savefig(filename2 + '.pdf', dpi=300)
