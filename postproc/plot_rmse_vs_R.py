#!/usr/bin/python
'''
File Name : plot_rmse_vs_R.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 19-03-2019
Modified: Tue 19 Mar 2019 11:59:33 AM CET
Purpose: plot rmse versus R for multiple ensembles
         read precalculated data

(c) 2018 under a MIT License (https://mit-license.org)
'''
import numpy as np
import xarray as xr
import logging
import os
import matplotlib.pyplot as plt

# INPUT
indir = '/net/tropo/climphys/rlorenz/Data/IntercomparisonRobustness/'
varname1 = 'tas'
varname2 = 'pr'
season = 'JJA'
scenarios = ['sresa2', 'rcp85']
base_start = '1986'
base_end = '2005'
future_start = '2081'
future_end = '2100'
obsdata = 'ERA-Interim'

cmip3_data1 = 'processed_CMIP3_data/%s/netcdf' %(varname1)
cmip5_data1 = 'processed_CMIP5_data/%s/netcdf' %(varname1)
w_cmip5_data1 = 'weighted_CMIP5_data/%s/netcdf' %(varname1)

cmip3_data2 = 'processed_CMIP3_data/%s/netcdf' %(varname2)
cmip5_data2 = 'processed_CMIP5_data/%s/netcdf' %(varname2)
w_cmip5_data2 = 'weighted_CMIP5_data/%s/netcdf' %(varname2)

# READ ROBUSTNESS
#tas
f_robustness_cmip3 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, cmip3_data1, future_start, future_end, scenarios[0], season)
R_cmip3_tas = xr.open_dataset(f_robustness_cmip3)['R']

f_robustness_cmip5 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, cmip5_data1, future_start, future_end, scenarios[1], season)
R_cmip5_tas = xr.open_dataset(f_robustness_cmip5)['R']

f_robustness_wcmip5 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data1, future_start, future_end, scenarios[1], season)
R_wcmip5_tas = xr.open_dataset(f_robustness_wcmip5)['R']

#pr
f_robustness_cmip3 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, cmip3_data2, future_start, future_end, scenarios[0], season)
R_cmip3_pr = xr.open_dataset(f_robustness_cmip3)['R']

f_robustness_cmip5 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, cmip5_data2, future_start, future_end, scenarios[1], season)
R_cmip5_pr = xr.open_dataset(f_robustness_cmip5)['R']

f_robustness_wcmip5 = '%s%s/R_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data2, future_start, future_end, scenarios[1], season)
R_wcmip5_pr = xr.open_dataset(f_robustness_wcmip5)['R']


# READ RMSE
#tas
f_cmip3_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip3_data1, obsdata, base_start, base_end, scenarios[0], season)
cmip3_rmse_tas = xr.open_dataset(f_cmip3_rmse)['rmse']

f_cmip5_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip5_data1, obsdata, base_start, base_end, scenarios[1], season)
cmip5_rmse_tas = xr.open_dataset(f_cmip5_rmse)['rmse']

f_w_cmip5_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data1, obsdata, base_start, base_end, scenarios[1], season)
w_cmip5_rmse_tas = xr.open_dataset(f_w_cmip5_rmse)['rmse']
# pr
f_cmip3_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip3_data2, obsdata, base_start, base_end, scenarios[0], season)
cmip3_rmse_pr = xr.open_dataset(f_cmip3_rmse)['rmse']

f_cmip5_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip5_data2, obsdata, base_start, base_end, scenarios[1], season)
cmip5_rmse_pr = xr.open_dataset(f_cmip5_rmse)['rmse']

f_w_cmip5_rmse = '%s%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data2, obsdata, base_start, base_end, scenarios[1], season)
w_cmip5_rmse_pr = xr.open_dataset(f_w_cmip5_rmse)['rmse']

# READ CORR
f_cmip3_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip3_data1, obsdata, base_start, base_end, scenarios[0], season)
cmip3_corr_tas = xr.open_dataset(f_cmip3_corr)['corr']

f_cmip5_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip5_data1, obsdata, base_start, base_end, scenarios[1], season)
cmip5_corr_tas = xr.open_dataset(f_cmip5_corr)['corr']

f_w_cmip5_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data1, obsdata, base_start, base_end, scenarios[1], season)
w_cmip5_corr_tas = xr.open_dataset(f_w_cmip5_corr)['corr']
#pr
f_cmip3_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip3_data2, obsdata, base_start, base_end, scenarios[0], season)
cmip3_corr_pr = xr.open_dataset(f_cmip3_corr)['corr']

f_cmip5_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, cmip5_data2, obsdata, base_start, base_end, scenarios[1], season)
cmip5_corr_pr = xr.open_dataset(f_cmip5_corr)['corr']

f_w_cmip5_corr = '%s%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(
    indir, w_cmip5_data2, obsdata, base_start, base_end, scenarios[1], season)
w_cmip5_corr_pr = xr.open_dataset(f_w_cmip5_corr)['corr']

# CALCULATE R aggregated over area (percentage of grid points R>0.9)
thres = 0.9
total_gp = R_cmip3_tas.shape[0] * R_cmip3_tas.shape[1]
#tas
cmip3_above_tas = R_cmip3_tas.where(R_cmip3_tas>thres, drop=True)
gp_above3_tas = total_gp - np.sum(np.isnan(cmip3_above_tas))
cmip3_gp_tas = gp_above3_tas / total_gp * 100.

cmip5_above_tas = R_cmip5_tas.where(R_cmip5_tas>thres, drop=True)
gp_above5_tas = total_gp - np.sum(np.isnan(cmip5_above_tas))
cmip5_gp_tas = gp_above5_tas / total_gp * 100.

cmip5w_above_tas = R_wcmip5_tas.where(R_wcmip5_tas>thres, drop=True)
gp_above5w_tas = total_gp - np.sum(np.isnan(cmip5w_above_tas))
w_cmip5_gp_tas = gp_above5w_tas / total_gp * 100.
#pr
cmip3_above_pr = R_cmip3_pr.where(R_cmip3_pr>thres, drop=True)
gp_above3_pr = total_gp - np.sum(np.isnan(cmip3_above_pr))
cmip3_gp_pr = gp_above3_pr / total_gp * 100.

cmip5_above_pr = R_cmip5_pr.where(R_cmip5_pr>thres, drop=True)
gp_above5_pr = total_gp - np.sum(np.isnan(cmip5_above_pr))
cmip5_gp_pr = gp_above5_pr / total_gp * 100.

cmip5w_above_pr = R_wcmip5_pr.where(R_wcmip5_pr>thres, drop=True)
gp_above5w_pr = total_gp - np.sum(np.isnan(cmip5w_above_pr))
w_cmip5_gp_pr = gp_above5w_pr / total_gp * 100.


# PLOT R versus RMSE
#title = '%s %s' %(varname, season)
filename = os.path.join(indir, 'Agg_Plots/R_vs_RMSE_{}_{}_{}_{}-{}'.format(varname1, varname2, season, future_start, future_end))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(cmip3_rmse_tas, cmip3_gp_tas, 'o', color='red', label='tas CMIP3 (16)')
plt.plot(cmip5_rmse_tas, cmip5_gp_tas, '>', color='red', label='tas CMIP5 (40)')
plt.plot(w_cmip5_rmse_tas, w_cmip5_gp_tas, 's', color='red', label='tas weighted CMIP5 (89)')
plt.plot(cmip3_rmse_pr, cmip3_gp_pr, 'o', color='blue', label='pr CMIP3 (16)')
plt.plot(cmip5_rmse_pr, cmip5_gp_pr, '>', color='blue', label='pr CMIP5 (40)')
plt.plot(w_cmip5_rmse_pr, w_cmip5_gp_pr, 's', color='blue', label='pr weighted CMIP5 (89)')
#ax.set_xlim(1., 5.)
ax.set_ylim(50., 100.)
ax.set_xlabel('RMSE', fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend()
plt.savefig(filename + '.pdf', dpi=300)

# PLOT R versus CORR
#title = '%s %s' %(varname, season)
filename = os.path.join(indir, 'Agg_Plots/R_vs_CORR_{}_{}_{}_{}-{}'.format(varname1, varname2, season, future_start, future_end))
fig, ax = plt.subplots(figsize=(8, 8))

plt.plot(cmip3_corr_tas, cmip3_gp_tas, 'o', color='red', label='tas CMIP3')
plt.plot(cmip5_corr_tas, cmip5_gp_tas, '>', color='red', label='tas CMIP5')
plt.plot(w_cmip5_corr_tas, w_cmip5_gp_tas, 's', color='red', label='tas weighted CMIP5')
plt.plot(cmip3_corr_pr, cmip3_gp_pr, 'o', color='blue', label='pr CMIP3')
plt.plot(cmip5_corr_pr, cmip5_gp_pr, '>', color='blue', label='pr CMIP5')
plt.plot(w_cmip5_corr_pr, w_cmip5_gp_pr, 's', color='blue', label='pr weighted CMIP5')

#ax.set_xlim(0., 0.01)
ax.set_ylim(50., 100.)
ax.set_xlabel('CORR(%s, historical)' %(obsdata), fontsize='large')
ax.set_ylabel('%% of grid points with R>%s' %(str(thres)), fontsize='large')
#ax.set_title(title)
ax.legend()
#plt.show()
plt.savefig(filename + '.pdf', dpi=300)
