#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
File Name: plotting.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 13-02-2019
Modified: Thu Dec 10 12:42:18 2020
Purpose: plotting functions


'''
import matplotlib as mpl
mpl.use('Agg')
import os
import logging
import numpy as np
import xarray as xr

import matplotlib as mpl
import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
from matplotlib.colors import ListedColormap

logger = logging.getLogger(__name__)

def create_uneven_colormap():
    '''
    Parameters
    ----------
    none

    Returns
    ----------
    colormap with uneven number of blues and reds
    '''
    # colors =  [(10, 40, 100), (51, 102, 217), (105, 138, 236), (151, 180, 250),
    #            (204, 217, 255), (255, 245, 204), (255, 224, 153),
    #            (255, 203, 102), (255, 180, 51), (255, 140, 51), (255, 85, 0),
    #            (230, 40, 30), (191, 0, 0), (140, 0, 0), (108, 0, 0),
    #            (110, 0, 70)]

    # bit_rgb = np.linspace(0,1,256)
    # for i in range(len(colors)):
    #     colors[i] = (bit_rgb[colors[i][0]],
    #                  bit_rgb[colors[i][1]],
    #                  bit_rgb[colors[i][2]])
    color_list = [(0.0392, 0.157, 0.392), (0.2, 0.4, 0.851),
                  (0.412, 0.541, 0.925), (0.592, 0.706, 0.980),
                  (0.8, 0.851, 1.0), (1.0, 0.961, 0.8),
                  (1.0, 0.878, 0.6), (1.0, 0.796, 0.4),
                  (1.0, 0.706, 0.2), (1.0, 0.549, 0.2), (1.0, 0.333, 0.0),
                  (0.902, 0.157, 0.118), (0.749, 0.0, 0.0),
                  (0.549, 0.0, 0.0), (0.424, 0.0, 0.0), (0.431, 0.0, 0.27)]
    cmap = ListedColormap(color_list)
    return cmap

def plot_stipphatch(data, lon, lat, ax, hatch_bounds=[0.0, 0.05],
                    hatch_styles='..'):
    """Plot the stippling/hatching."""
    index = np.where((data>hatch_bounds[0]) & (data<=hatch_bounds[1]))

    logger.info('contourf')
    ax.contourf(lon, lat, data, transform=ccrs.PlateCarree(),
                colors='none', levels=hatch_bounds,
                hatches=hatch_styles, alpha=0.0)

def plot_map_stip_hatch(da, robustness, significance, path, attributes):
    """
    Plot map with stippling and hatching

    Parameters
    ----------
    da: xarray.DataArray, shape (N, N)
    robustness: np.array, shape (N, N)
    significance: xarray.DataArray, shape (M, N, N)
    path: string
        where to save the plot

    Returns
    -------
    filename: string
        name of the file where plot is stored
    """

    proj = ccrs.Mollweide()
    fig, ax = plt.subplots(subplot_kw={'projection': proj})
    if attributes['varname'] == 'tas':
        contour_levels = [-2.,-1.5,-1.0,-0.5,0.,0.5,1.,1.5,2.,3.,4.,5.,7.,11.]
        cmap = create_uneven_colormap()
    elif attributes['varname'] == 'pr':
        contour_levels = [-80,-40,-20,-10,-5,-2.5,0,2.5,5,10,20,40,80]
        cmap = mpl.cm.get_cmap('BrBG')

    da.plot.pcolormesh('lon', 'lat',
        ax=ax, transform=ccrs.PlateCarree(), levels=contour_levels,
        cmap=cmap, extend='both',
        cbar_kwargs={'orientation': 'horizontal',
                     'label': '%s (%s)' %(attributes['varname'],
                                          attributes['unit']),
                     'pad': .1})

    ax.coastlines(linewidth=0.5)

    lon = da['lon']
    lat = da['lat']

    if np.count_nonzero(np.where(robustness>0.8)) != 0:
        plot_stipphatch(robustness, lon, lat, ax, hatch_bounds=[0.8, 0.95],
                        hatch_styles='..')
        if np.count_nonzero(np.where(robustness>0.95)) != 0:
            plot_stipphatch(robustness, lon, lat, ax, hatch_bounds=[0.95, 1.0],
                            hatch_styles='oo')

    if np.count_nonzero(significance) != 0:
        plot_stipphatch(significance, lon, lat, ax, hatch_bounds=[0.01, 1.0],
                        hatch_styles='x')

    ax.set_global()

    try:
        ax.set_title('$\Delta${} {} {}:{}-{}'.format(attributes['varname'],
                                                     attributes['season'].upper(),
                                                     attributes['scenario'].upper(),
                                                     attributes['syear_fut'],
                                                     attributes['eyear_fut']))
        filename = os.path.join(path, 'map_{}_{}_{}_{}_{}-{}_minus_{}-{}'.format(
                                attributes['varname'], 'CLIM', attributes['season'],
                                attributes['scenario'],
                                attributes['syear_fut'], attributes['eyear_fut'],
                                attributes['syear_base'], attributes['eyear_base']))
    except(KeyError):
        degree_sign= u'\N{DEGREE SIGN}'
        warm = attributes['warm_lev']
        digits = [int(d) for d in str(warm)]
        warm_str = '{}.{}{}C'.format(digits[0], digits[1], degree_sign)
        ax.set_title('$\Delta${} {} {}: warming level {}'.format(attributes['varname'],
                                                     attributes['season'].upper(),
                                                     attributes['scenario'].upper(),
                                                     warm_str))
        filename = os.path.join(path, 'map_{}_{}_{}_{}_warming_level_{}'.format(
                                attributes['varname'], 'CLIM', attributes['season'],
                                attributes['scenario'],
                                attributes['warm_lev']))


    plt.savefig(filename + '.png', dpi=300)
    plt.savefig(filename + '.pdf', dpi=300)
    plt.close()
    logger.debug('Saved plot: {}.pdf'.format(filename))

    return filename
