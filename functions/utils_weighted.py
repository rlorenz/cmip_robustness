#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
File Name: utils_weighted.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 14-12-2020
Modified: Mon Dec 14 12:27:42 2020
Purpose: utility functions for weighted model ensembles

(c) 2018 under a MIT License (https://mit-license.org)
'''
import numpy as np

def as_repeats(self):
    """get array that has repeats given by floor(weights)

    observations with weight=0 are dropped

    """
    w_int = np.floor(self.weights).astype(int)
    return np.repeat(self.data, w_int, axis=0)

def as_repeats_3D(data, weights, blow_nr=1000):
    """get array that has repeats given by floor(weights)

    for 3D data arrays with dimensions (model, lat, lon)
    """
    data_weights = np.empty((blow_nr, len(data['lat']),
                           len(data['lon']))) * np.nan
    i_we=0
    for i in range(0, len(data)):
        iweight = np.floor(weights[i].data * blow_nr).astype(int)
        ind_we = i_we + iweight

        data_weights[i_we:ind_we,] = np.repeat(data[i].data[np.newaxis,:,:],
                                               iweight, axis=0)
        i_we = ind_we

    return data_weights, ind_we

def weighted_quantile(values: list,
                      quantiles: list,
                      weights: list = None) -> 'np.array':
    """Calculate weighted quantiles.
    Analogous to np.quantile, but supports weights.
    Based on: https://stackoverflow.com/a/29677616/6012085
    Parameters
    ----------
    values: array_like
        List of input values.
    quantiles: array_like
        List of quantiles between 0.0 and 1.0.
    weights: array_like
        List with same length as `values` containing the weights.
    Returns
    -------
    np.array
        Numpy array with computed quantiles.
    """
    values = np.array(values)
    quantiles = np.array(quantiles)
    if weights is None:
        weights = np.ones(len(values))
    weights = np.array(weights)

    if not np.all((quantiles >= 0) & (quantiles <= 1)):
        raise ValueError('Quantiles should be between 0.0 and 1.0')

    idx = np.argsort(values)
    values = values[idx]
    weights = weights[idx]

    weighted_quantiles = np.cumsum(weights) - 0.5 * weights

    # Cast weighted quantiles to 0-1 To be consistent with np.quantile
    min_val = weighted_quantiles.min()
    max_val = weighted_quantiles.max()
    weighted_quantiles = (weighted_quantiles - min_val) / max_val

    return np.interp(quantiles, weighted_quantiles, values)
