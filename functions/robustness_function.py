#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
File Name : robustness_function.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 30-01-2019
Modified: Wed 30 Jan 2019 12:20:10 PM CET
Purpose:

(c) 2018 under a MIT License (https://mit-license.org)
'''
import numpy as np
import xarray as xr
import logging
import random
from scipy import stats
import timeit
from utils_python.decorators import vectorize

if __name__ == '__main__':
    from functions.utils_weighted import as_repeats_3D
else:
    from intercomparison_robustness_CMIP.functions.utils_weighted import (
    as_repeats_3D,
    )

logger = logging.getLogger(__name__)

def cumfrec(data, bins, limits):

    hist, __ = np.histogram(data, bins=bins, range=limits)
    return np.cumsum(hist)

def cumfrec2(data, bins, limit1, limit2):

    hist, __ = np.histogram(data, bins=bins, range=(limit1, limit2))
    return np.cumsum(hist)

def robustness_function(mean_base, mean_perturbed, std_base, std_perturbed,
                        weights, max_loops=100, anomalies=True, weighted=False):
    """
    Calculate robustness measure R of projections.
    based on signal-to-variability ratio and
    ranked probability skill score.
    Considers magnitude of change, sign, natural
    variability and inter-model spread.
    See Knutti and Sedlacek, 2013, NCC.

    Parameters
    ----------
    mean_base: numeric, xarray [time, lat, lon]
        The mean value over the base period for all models as lat, lon arrays

    mean_perturbed: numeric, xarray [time, lat, lon]
        The mean value over the perturbed/future period

    std_base: numeric, xarray [time, lat, lon]
        The standard deviation over the base period

    std_perturbed: numeric, xarray [time, lat, lon]
        The standard deviation over the perturbed/future period

    weights: xarray [model]
        The weight a model should obtain based on its performance
        and independence, all 1 for unweighted ensemble

    max_loops: numeric, optional
        The maximum number of loops performed (higher values can improve
        accuracy). Stopp early if std between R estimates smaller than

    anomalies: boolean, optional
        Flag that determines if the mean anomalies between base period
        and perturbed are used.

    weighted: boolean, optional
        Flag that determines if weights are != 1

    Returns
    ----------
    R: numeric
        Robustness measure R = 1-A1/A2
        A1 and A2 calculated using linear interpolation (rpss) or not (rpss2)
    """
#    print(
#        "mean_base: " + str(mean_base.shape) + " | mean_perturbed:" + str(mean_perturbed.shape) +
#        " | weights: " + str(weights.shape)
#    )
#    profile = cProfile.Profile()
#    profile.enable()
    if anomalies==True:
        mean_perturbed = mean_perturbed - mean_base
        mean_base = mean_base * 0.

    # Number of values to generate for baseline and future
    ny = 40

    accuracy = .001
    accuracy = 1/accuracy

    if np.isclose(weights.sum(), 1.0) or weighted:
        repetitions = np.ceil(weights * accuracy).astype(int)
    else:
        repetitions = weights

    new_model_length = int(repetitions.sum())

    rpss = list()

    for nl in range(0, max_loops):
        # Generate samples from random normally distributed numbers
        logger.debug('Generate random normally distributed samples, loop %s' %(
            nl))
        cv = np.empty((new_model_length, ny)) * np.nan
        c2v = np.empty((new_model_length, ny)) * np.nan
        fv = np.empty((new_model_length, ny)) * np.nan
        f2v = np.empty((new_model_length, ny)) * np.nan

        i_we = 0
        for i in range(0, len(mean_base)):
            if weighted:
                iweight = repetitions[i]
                ind_we = i_we + iweight

                tmp_cv = np.random.normal(mean_base[i], std_base[i], size=(ny))
                cv[i_we:ind_we,] = np.repeat(tmp_cv[np.newaxis, :], iweight, axis=0)

                tmp_c2v = np.random.normal(np.average(mean_base, axis=0, weights=weights),
                                           std_base[i], size=(ny))
                c2v[i_we:ind_we,] = np.repeat(tmp_c2v[np.newaxis, :], iweight, axis=0)

                tmp_fv = np.random.normal(mean_perturbed[i], std_perturbed[i], size=(ny))
                fv[i_we:ind_we, ] = np.repeat(tmp_fv[np.newaxis, :], iweight, axis=0)

                tmp_f2v = np.random.normal(np.average(mean_perturbed, axis=0, weights=weights),
                                           std_perturbed[i], size=(ny))
                f2v[i_we:ind_we,] = np.repeat(tmp_f2v[np.newaxis, :], iweight, axis=0)

                i_we = ind_we
            else:
                cv[i,] = np.random.normal(mean_base[i], std_base[i], size=(ny))
                c2v[i,] = np.random.normal(np.average(mean_base, axis=0),
                                           std_base[i], size=(ny))
                fv[i,] = np.random.normal(mean_perturbed[i], std_perturbed[i], size=(ny))
                f2v[i,] = np.random.normal(np.average(mean_perturbed, axis=0),
                                           std_perturbed[i], size=(ny))

        cvall = cv.ravel()
        fvall = fv.ravel()
        cv2all = c2v.ravel()
        fv2all = f2v.ravel()

        # Min max range of all sampled values
        maxall = np.nanmax([cvall, cv2all, fvall, fv2all])
        minall = np.nanmin([cvall, cv2all, fvall, fv2all])

        # Calculate cumulative density functions
        cvcdf = cumfrec(cvall, 2*ny, (minall, maxall))
        cvcdf2 = cumfrec(cv2all, 2*ny, (minall, maxall))
        fvcdf = cumfrec(fvall, 2*ny, (minall, maxall))
        fvcdf2 = cumfrec(fv2all, 2*ny, (minall, maxall))

        # Skill scores
        rpsm = np.nansum((fvcdf2 - fvcdf) **2) / len(cvcdf)
        rpsc = np.nansum((fvcdf2 - cvcdf2) **2) / len(cvcdf)
        rpss.append(1 - rpsm / rpsc)

        #fv2_fv = np.asarray(fv2all) - np.asarray(fvall)
        #rpsm2 = np.nansum((fv2_fv) * (fv2_fv)) / len(cvall)
        #fv2_cv2 = np.asarray(fv2all) - np.asarray(cv2all)
        #rpsc2 = np.nansum((fv2_cv2) * (fv2_cv2)) / len(cvall)
        #rpss2.append(1 - rpsm2 / rpsc2)

        if nl > 3:
            if np.std(rpss) < 0.0001:
                logger.info('std over rpss smaller than 0.0001, break at loop %s' %nl)
                break

    return np.nanmedian(rpss)#, np.nanmedian(rpss2)

#@vectorize('(t,n,m)->(n,m)')
def robustness_function_3D(mean_base, mean_perturbed,
                           std_base, std_perturbed, weights,
                           max_loops=100, anomalies=True, interp=True):
    """
    Calculate robustness measure R of projections.
    based on signal-to-variability ratio and
    ranked probability skill score.
    Considers magnitude of change, sign, natural
    variability and inter-model spread.
    See Knutti and Sedlacek, 2013, NCC.

    Parameters
    ----------
    mean_base: xarray [time, lat, lon]
        The mean value over the base period for all models as lat, lon arrays

    mean_perturbed: xarray [time, lat, lon]
        The mean value over the perturbed/future period

    std_base: xarray [time, lat, lon]
        The standard deviation over the base period

    std_perturbed: xarray [time, lat, lon]
        The standard deviation over the perturbed/future period

    max_loops: numeric, optional
        The number of loops, higher values can improve accuracy

    anomalies: boolean, optional
        Flag that determines if the mean anomalies between base period
        and perturbed are used.

    interp: boolean, optional
        Calculate R using linear interpolation? if False quicker, only rpss2

    weights: xarray [model]
        The weight a model should obtain based on its performance
        and independence, all 1 for unweighted ensemble

    Returns
    ----------
    R2, R: numeric
        Robustness measure R = 1-A1/A2
        A1 and A2 calculated using linear interpolation (rpss) or not (rpss2)
    """

    if anomalies==True:
        mean_perturbed = mean_perturbed - mean_base
        mean_base = mean_base * 0.

    nlat = len(mean_base['lat'])
    nlon = len(mean_base['lon'])

    # Number of values to generate for baseline and future
    ny = 50

    # Total number of values (based on number of models)
    # for weighted include appropriate number of times
    if np.isclose(weights.sum(), 1.0):
        we = True
        new_model_length = 1000
    else:
        we = False

    rpss = np.empty((max_loops, nlat, nlon)) * np.nan

    for nl in range(0, max_loops):
        # Generate samples from random normally distributed numbers
        logger.debug('Generate random normally distributed samples, loop %s' %(
            nl))

        # for weighted include appropriate number of times
        if we:
            new_model_length = 1000
            cv = np.empty((new_model_length, ny, nlat, nlon)) * np.nan
            c2v = np.empty((new_model_length, ny, nlat, nlon)) * np.nan
            fv = np.empty((new_model_length, ny, nlat, nlon)) * np.nan
            f2v = np.empty((new_model_length, ny, nlat, nlon)) * np.nan
            i_we = 0
        else:
            # Control period
            # fill into array, and reshape into a vector
            cv = np.empty((len(mean_base), ny, nlat, nlon)) * np.nan
            c2v = np.empty((len(mean_base), ny, nlat, nlon)) * np.nan
            # Same for future period but assuming all models predict the same
            # present day and mean change
            fv = np.empty((len(mean_perturbed), ny, nlat, nlon)) * np.nan
            f2v = np.empty((len(mean_perturbed), ny, nlat, nlon)) * np.nan

        for i in range(0, len(mean_base)):
            if we:
                tmp_cv = np.random.normal(mean_base.data[i, ],
                                          std_base.data[i,],
                                          size=(ny,nlat,nlon))
                iweight = int(np.floor(weights[i].data * new_model_length))
                ind_we = i_we + iweight

                cv[i_we:ind_we,] = np.repeat(tmp_cv[np.newaxis,:,:,:], iweight,
                                             axis=0)

                tmp_c2v = np.random.normal(np.average(mean_base, axis=0,
                                                      weights=weights),
                                           std_base.data[i,],
                                           size=(ny,nlat,nlon))
                c2v[i_we:ind_we,] = np.repeat(tmp_c2v[np.newaxis,:,:,:],
                                              iweight, axis=0)

                tmp_fv = np.random.normal(mean_perturbed[i,],
                                           std_perturbed[i,],
                                           size=(ny, nlat, nlon))
                fv[i_we:ind_we,] = np.repeat(tmp_fv[np.newaxis,:,:,:], iweight,
                                             axis=0)
                tmp_f2v = np.random.normal(np.average(mean_perturbed, axis=0,
                                                       weights=weights),
                                            std_perturbed[i,],
                                            size=(ny, nlat, nlon))
                f2v[i_we:ind_we,] = np.repeat(tmp_f2v[np.newaxis,:,:,:],
                                              iweight, axis=0)
                i_we = ind_we
            else:
                cv[i,] = np.random.normal(mean_base.data[i, ],
                                          std_base.data[i,],
                                          size=(ny,nlat,nlon))
                c2v[i,] = np.random.normal(np.average(mean_base, axis=0),
                                           std_base.data[i,],
                                           size=(ny,nlat,nlon))
                fv[i, ] = np.random.normal(mean_perturbed[i,],
                                           std_perturbed[i,],
                                           size=(ny, nlat, nlon))
                f2v[i, ] = np.random.normal(np.average(mean_base, axis=0) +
                                            np.average(mean_perturbed-mean_base,
                                                       axis=0),
                                            std_perturbed[i,],
                                            size=(ny, nlat, nlon))

        cvall = cv.reshape(-1, *cv.shape[-2:])
        c2vall =  c2v.reshape(-1, *c2v.shape[-2:])

        fvall = fv.reshape(-1, *fv.shape[-2:])
        f2vall = f2v.reshape(-1, *f2v.shape[-2:])

        # Min max range of all sampled values
        maxall = np.nanmax([np.nanmax(cvall), np.nanmax(c2vall), np.nanmax(fvall), np.nanmax(f2vall)])
        minall = np.nanmin([np.nanmin(cvall), np.nanmin(c2vall), np.nanmin(fvall), np.nanmin(f2vall)])

        # Calculate cumulative density functions # how to vectorize correctly???
        vcumfrec = np.vectorize(cumfrec2)
        cvcdf = vcumfrec(cvall, 2*ny, minall, maxall)
        cvcdf2 = vcumfrec(c2vall, 2*ny, minall, maxall)
        fvcdf = vcumfrec(fvall, 2*ny, minall, maxall)
        fvcdf2 = vcumfrec(f2vall, 2*ny, minall, maxall)
        print(cvcdf.shape)
        # Skill scores
        rpsm = np.nansum((fvcdf2 - fvcdf) **2) / len(cvcdf)
        rpsc = np.nansum((fvcdf2 - cvcdf2) **2) / len(cvcdf)
        rpss[nl,] = 1 - rpsm / rpsc

    return np.median(rpss, axis=0)


#@guvectorize(
#    "(float64[:], float64[:], float64[:], float64[:], float64[:], int64, float64)",
#    "(n), (n), (n), (n), (n), () -> ()",
#    nopython=True,
#)
def robustness_function_gufunc(mean_base, mean_perturbed, std_base, std_perturbed,
                               weights, max_loops, out):
    """
    Calculate robustness measure R of projections.
    based on signal-to-variability ratio and
    ranked probability skill score.
    Considers magnitude of change, sign, natural
    variability and inter-model spread.
    See Knutti and Sedlacek, 2013, NCC.

    Parameters
    ----------
    mean_base: numeric [models]
        The mean value over the base period for all models as lat, lon arrays

    mean_perturbed: numeric [models]
        The mean value over the perturbed/future period

    std_base: numeric [models]
        The standard deviation over the base period

    std_perturbed: numeric [models]
        The standard deviation over the perturbed/future period

    weights: numeric [models]
        The weight a model should obtain based on its performance
        and independence, all 1 for unweighted ensemble

    max_loops: numeric
        The number of loops, higher values can improve accuracy

    out: numeric
        Robustness measure R


    Returns
    ----------
    R: numeric
        Robustness measure R = 1-A1/A2
        A1 and A2 calculated using linear interpolation (rpss) or not (rpss2)
    """
    def sample_distribution_2(means, stds, nx):
        #r = np.random.randn(nx, *means.shape) * stds + means
        shape = (nx,) + means.shape
        r = np.random.normal(means, stds, size=shape)
        return r

    def cumfrec(data, bins, limits):

        hist, __ = np.histogram(data, bins=bins, range=limits)
        return np.cumsum(hist)

#    if anomalies==True:
#        mean_perturbed = mean_perturbed - mean_base
#        mean_base = mean_base * 0.

    # Number of values to generate for baseline and future
    ny = 40

    accuracy = .001
    accuracy = 1/accuracy

    if weights.sum()!=len(mean_perturbed):
        repetitions = np.ceil(weights * accuracy) #.astype(int)
    else:
        repetitions = weights

    means_base = np.repeat(mean_base, repetitions)
    stds_base = np.repeat(std_base, repetitions)
    means_perturbed = np.repeat(mean_perturbed, repetitions)
    stds_perturbed = np.repeat(std_perturbed, repetitions)

    rpss = list()

    # func(mean: array, std: array, ny: int) -> dist: list
    sample_distribution = np.vectorize(np.random.normal, signature='(),(),()->(n)')

    for nl in range(0, max_loops):
        # Generate samples from random normally distributed numbers
        #logger.debug('Generate random normally distributed samples, loop %s' %(
        #    nl))

        cv = sample_distribution_2(means_base, stds_base, ny)
        fv = sample_distribution_2(means_perturbed, stds_perturbed, ny)

        cv2 = sample_distribution(means_base.mean(), stds_base, ny)
        fv2 = sample_distribution(means_perturbed.mean(), stds_perturbed, ny)

        cvall = cv.ravel()
        fvall = fv.ravel()
        cv2all = cv2.ravel()
        fv2all = fv2.ravel()

        # Min max range of all sampled values
        maxall = np.nanmax([cvall, cv2all, fvall, fv2all])
        minall = np.nanmin([cvall, cv2all, fvall, fv2all])

        # Calculate cumulative density functions
        cvcdf = cumfrec(cvall, 2*ny, (minall, maxall))
        cvcdf2 = cumfrec(cv2all, 2*ny, (minall, maxall))
        fvcdf = cumfrec(fvall, 2*ny, (minall, maxall))
        fvcdf2 = cumfrec(fv2all, 2*ny, (minall, maxall))

        # Skill scores
        rpsm = np.nansum((fvcdf2 - fvcdf) **2) / len(cvcdf)
        rpsc = np.nansum((fvcdf2 - cvcdf2) **2) / len(cvcdf)
        rpss.append(1 - rpsm / rpsc)

        if nl > 3:
            if np.std(rpss) < 0.001:
                #logger.info('std over rpss smaller than 0.001, break at loop %s' %nl)
                break

    out[:] = np.nanmedian(rpss)
