#!/usr/bin/python
'''
File Name : calc_performance.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 18-03-2019
Modified: Mon 18 Mar 2019 02:39:50 PM CET
Purpose: calculate performance of ensemble to be compared with robustess R

(c) 2018 under a MIT License (https://mit-license.org)
'''
import numpy as np
import xarray as xr
import logging
import os
import argparse
from scipy import stats

from utils_python import utils
from utils import calc_RMSE_obs_mod_3D
from utils_python.xarray import add_hist, area_weighted_mean
from utils_python.decorators import vectorize

if __name__ == '__main__':
    from multi_model_robustness import (
        test_config, read_args, read_config, set_up_filenames, calc_data,
        calc_stats, read_weights,
        )
else:
    from intercomparison_robustness_CMIP.multi_model_robustness import (
	test_config, read_args, read_config, set_up_filenames, calc_data,
        calc_stats, read_weights,
	)

logger = logging.getLogger(__name__)

def standardize_units(da, varn):
    """Convert units to a common standard"""
    if 'units' in da.attrs.keys():
        unit = da.attrs['units']
        attrs = da.attrs
    else:
        logmsg = 'units attribute not found for {}'.format(varn)
        logger.warning(logmsg)
        return None

    # --- precipitation ---
    if varn == 'pr':
        newunit = 'mm/day'
        if unit == 'kg m-2 s-1':
            da.data *= 24*60*60
            da.attrs = attrs
            da.attrs['units'] = newunit
        elif unit == 'mm':  # E-OBS
            da.attrs['units'] = newunit
        elif unit == newunit:
            pass
        else:
            logmsg = 'Unit {} not covered for {}'.format(unit, varn)
            raise ValueError(logmsg)

    # --- temperature ---
    elif varn in ['tas', 'tasmax', 'tasmin', 'tos']:
        newunit = "degC"
        if unit == newunit:
            pass
        elif unit == 'K':
            da.data -= 273.15
            da.attrs = attrs
            da.attrs['units'] = newunit
        elif unit.lower() in ['degc', 'deg_c', 'celsius', 'degreec',
                              'degree_c', 'degree_celsius']:
            # https://ferret.pmel.noaa.gov/Ferret/documentation/udunits.dat
            da.attrs['units'] = newunit
        else:
            logmsg = 'Unit {} not covered for {}'.format(unit, varn)
            raise ValueError(logmsg)
    # --- not covered ---
    else:
        logmsg = 'Variable {} not covered in standardize_units'.format(varn)
        logger.warning(logmsg)

    return da

def main(args):
    """Call functions""" 
    logger.info('main().read_config()')
    cfg = read_config(args)
    print(cfg)
    files, model_ensembles = set_up_filenames(cfg, all_members = cfg.ensembles)

    logger.info('Calculating Stats')
    da_base, da_mean_base, da_std_base = calc_data(
        files, model_ensembles, cfg, cfg.base_start, cfg.base_end)
    da_mean_base = standardize_units(da_mean_base, cfg.varname)

    obs_files = '%s%s_mon_%s_g025.nc' %(cfg.obs_path, cfg.varname, cfg.obsdata)

    ds_obs = calc_stats(obs_files, cfg.varname, cfg.save_path,
                        time_period=(cfg.base_start, cfg.base_end),
                        season=cfg.season, time_aggregation='CLIM',
                        overwrite=cfg.overwrite)
    da_obs = standardize_units(ds_obs[cfg.varname], cfg.varname)

    if cfg.weights:
        # read weights
        model_weights = read_weights(list(model_ensembles), cfg)
        assert np.isclose(model_weights.sum(), 1.0), "sum of model weights != 1"
        # Calculate weighted multi-model mean
        da_mean_base_w = da_mean_base * model_weights
        da_mean_base_mmm = da_mean_base_w.sum(axis=0)
    else:
        # Calculate multi-model mean
        da_mean_base_mmm = da_mean_base.mean(axis=0)
 
    diff = (da_obs - da_mean_base_mmm)**2
    rmse_avg = np.sqrt(diff.mean())
    logger.info('global RMSE: %s' %rmse_avg.data)

    rmse_func = calc_RMSE_obs_mod_3D.rmse_3D(da_obs.data,
                                             da_mean_base_mmm.data,
                                             da_base['lat'].data,
                                             da_base['lon'].data)
    da_rmse_func = xr.DataArray(rmse_func)
    logger.info('RMSE lat/lon weighted: %s' %rmse_func)

    corr = np.corrcoef(da_obs.data.flatten(),
                       da_mean_base_mmm.data.flatten())

    da_corr = xr.DataArray(corr[1, 0])
    logger.info('Corr: %s '%da_corr.data)

    # Save data to netcdfs
    logger.info('Saving data to netcdf')
    outdir = '%s/%s/netcdf' %(cfg.save_path, cfg.varname)
    os.makedirs(outdir, exist_ok=True)

    outfile2 = '%s/RMSE_areaavg_%s_%s-%s_%s_%s.nc' %(outdir, cfg.obsdata,
                                                     cfg.base_start,
                                                     cfg.base_end, cfg.scenario,
                                                     cfg.season)
    ds_rmse_avg = da_rmse_func.to_dataset(name='rmse')
    ds_rmse_avg.to_netcdf(path=outfile2, mode='w')

    outfile3 = '%s/CORR_areaavg_%s_%s-%s_%s_%s.nc' %(outdir, cfg.obsdata,
                                                     cfg.base_start,
                                                     cfg.base_end, cfg.scenario,
                                                     cfg.season)
    ds_corr = da_corr.to_dataset(name='corr')
    ds_corr.to_netcdf(path=outfile3, mode='w')

if __name__ == "__main__":
    args = read_args()
    utils.set_logger()
    main(args)
