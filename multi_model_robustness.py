#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
File Name : multi_model_robustness.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 30-01-2019
Modified: Thu Dec 17 14:56:09 2020
Purpose: Main file to run robustness comparison for CMIP

References:
Knutti, R., J. Sedláček (2013), Robustness and uncertainties in the new CMIP5
climate model projections, Nature Climate Change, 3, pages 369–373,
doi: 10.1038/NCLIMATE1716.

(c) 2020 under a MIT License (https://mit-license.org)
'''
import numpy as np
import xarray as xr
import logging
import os
import warnings
import argparse
from natsort import natsorted
import time

from scipy import stats, signal
from statsmodels.stats.multitest import fdrcorrection, multipletests

from utils_python.decorators import vectorize
from utils_python import utils
from utils_python.get_filenames import Filenames
from utils_python.xarray import standardize_dimensions, area_weighted_mean

if __name__ == '__main__':
    from functions.robustness_function import (
        robustness_function, #robustness_function_3D,
        )
    from functions.utils_weighted import as_repeats_3D
    from functions.plotting import plot_map_stip_hatch
else:
    from intercomparison_robustness_CMIP.functions.robustness_function import (
	robustness_function, #robustness_function_3D,
	)
    from intercomparison_robustness_CMIP.functions.utils_weighted import (
    as_repeats_3D,
    )
    from intercomparison_robustness_CMIP.functions.plotting import (
	plot_map_stip_hatch,
	)

logger = logging.getLogger(__name__)

def test_config(cfg):
    """Some basic consistency tests for the config input"""
    if not os.access(cfg.save_path, os.W_OK | os.X_OK):
        raise ValueError('save_path is not writable')
    if not np.all([isinstance(cfg.overwrite, bool),
                   isinstance(cfg.debug, bool),
                   isinstance(cfg.plot, bool)]):
        raise ValueError('Typo in overwrite, debug, plot?')
    return None


def read_args():
    """Read the given configuration from the config file"""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        dest='config', nargs='?', default='DEFAULT',
        help='Name of the configuration to use (optional).')
    parser.add_argument(
        '--filename', '-f', dest='filename', default='configs/config.ini',
        help='Relative or absolute path/filename.ini of the config file.')
    parser.add_argument(
        '--logging-level', '-log-level', dest='log_level', default=20,
        type=str, choices=['error', 'warning', 'info', 'debug'],
        help='Set logging level')
    parser.add_argument(
        '--logging-file', '-log-file', dest='log_file', default=None,
        type=str, help='Redirect logging output to given file')
    return parser.parse_args()


def read_config(args):
    cfg = utils.read_config(args.config, args.filename)
    utils.log_parser(cfg)
    test_config(cfg)
    return cfg

#@vectorize('(n)->(n)')
def detrend(data):
    if np.any(np.isnan(data)):
        return data * np.nan
    return signal.detrend(data)

def set_up_filenames(cfg, all_members=False):
    """
    Return a list of filenames.

    Parameters
    ----------
    cfg : object
        A config object, must include
        project: str
        data_path : str
            Path to data
        scenario : str
            Which scenario to look for in data_path
        varname : str
            A valid variable name
        freq : str
            Frequency of files in data_path to use (mon, sea, ann)
    all_members=False : bool, optional
        If True include all available ensemble members per model. If False
        include only one (the first) member.

    Returns
    -------
    filenames : tuple
        A tuple of filenames
    """

    model_ensemble, filenames = (), ()

    if ((cfg.project == 'CMIP5') or (cfg.project == 'CMIP3')):
        fn = Filenames(
            file_pattern='{varn}/{varn}_%s_{model}_{scenario}_{ensemble}_g025.nc' %(
                cfg.freq), base_path=cfg.data_path)
    elif (cfg.project == 'CMIP6'):
        fn = Filenames(
            file_pattern='{varn}/%s/g025/{varn}_%s_{model}_{scenario}_{ensemble}_g025.nc' %(
                cfg.freq, cfg.freq), base_path=cfg.data_path)
    else:
        logger.error('Wrong project name in config, must be CMIP 3, 5 or 6')

    fn.apply_filter(varn=cfg.varname, scenario=cfg.scenario)
    try:
        models = cfg.models
        logger.info('Models read from config')
    except AttributeError:
        models = fn.get_variable_values('model', subset={'varn': cfg.varname})

    for mod in models:
        # ensemble members available for this model & variable
        ensembles_var = fn.get_variable_values(
            'ensemble', subset={'scenario': cfg.scenario,
                                'model': mod,
                                'varn': cfg.varname})
        if (cfg.project == 'CMIP6'):
            # check that also available for historical
            ensembles_hist = fn.get_variable_values(
                'ensemble', subset={'scenario': 'historical',
                                    'model': mod,
                                    'varn': cfg.varname})
            ensembles_var = set(ensembles_var).intersection(ensembles_hist)

        if not all_members:
            ensembles_select = natsorted(ensembles_var)[:1]
        else:
            ensembles_select = ensembles_var

        for ensemble in ensembles_select:
            model_ensemble += ('{}_{}_{}'.format(mod, ensemble, cfg.project),)
            ff = fn.get_filenames(
                subset={'varn': cfg.varname,
                        'model': mod,
                        'scenario': cfg.scenario,
                        'ensemble': ensemble})
            assert len(ff) == 1, 'len(ff) should be one!'
            filenames += (ff[0],)

    logger.info('{} files found.'.format(len(filenames)))
    logger.info(model_ensemble)
    return filenames, model_ensemble

def calc_data(files, models, cfg, start, end):
    data_times = []
    data_means = []
    data_stds = []
    path = os.path.join(cfg.save_path, cfg.varname, cfg.freq)
    os.makedirs(path, exist_ok=True)

    for filename, model_ensemble in zip(files, models):
        data_time = calc_stats(filename, cfg.varname, cfg.scenario, cfg.project,
                               path,
                               time_period=(start, end),
                               season=cfg.season,
                               time_aggregation='YEAR',
                               overwrite=cfg.overwrite)
        # in some models years (e.g. 2100) missing, enforce all have same length
        nyears = end - start + 1
        nyears_dat = len(data_time['year'])
        if nyears_dat != nyears:
            nlat = len(data_time['lat'])
            nlon = len(data_time['lon'])
            tmp_ar = np.empty((nyears, nlat, nlon)) * np.nan
            if data_time['year'][0] == start:
                tmp_ar[0:nyears_dat, :, :] = data_time[cfg.varname]
            else:
                logger.error('Missing data in %s' %(filename))
                continue
            year_coord = list(range(start, end + 1))
            data_time = xr.DataArray(tmp_ar, coords={'year': year_coord,
                                                     'lat': data_time['lat'],
                                                     'lon': data_time['lon']},
                                     dims=('year', 'lat', 'lon'))
            data_time = data_time.to_dataset(name=cfg.varname)

        data_time['model_ensemble'] = xr.DataArray([model_ensemble],
                                                   dims='model_ensemble')
        if (cfg.varname == 'tas' and cfg.project == 'CMIP6'):
            try:
                data_time = data_time.drop('height')
            except ValueError:
                pass

        data_times.append(data_time)

        data_mean = calc_stats(filename, cfg.varname, cfg.scenario, cfg.project,
                               path,
                               time_period=(start, end),
                               season=cfg.season,
                               time_aggregation='CLIM',
                               overwrite=cfg.overwrite)
        data_mean['model_ensemble'] = xr.DataArray([model_ensemble],
                                                   dims='model_ensemble')
        if (cfg.varname == 'tas' and cfg.project == 'CMIP6'):
            try:
                data_mean = data_mean.drop('height')
            except ValueError:
                pass
        data_means.append(data_mean)
        data_std = calc_stats(filename, cfg.varname, cfg.scenario, cfg.project,
                              path,
                              time_period=(start, end),
                              season=cfg.season,
                              time_aggregation='STD',
                              overwrite=cfg.overwrite)
        data_std['model_ensemble'] = xr.DataArray([model_ensemble],
                                                   dims='model_ensemble')
        if (cfg.varname == 'tas' and cfg.project == 'CMIP6'):
            try:
                data_std = data_std.drop('height')
            except ValueError:
                pass

        data_stds.append(data_std)
        logger.debug('Calculate diagnostics for file {}... DONE'.format(
            filename))

    return (xr.concat(data_times, dim='model_ensemble')[cfg.varname],
            xr.concat(data_means, dim='model_ensemble')[cfg.varname],
            xr.concat(data_stds, dim='model_ensemble')[cfg.varname])

def calc_stats(infile, varn, scenario, proj, outpath,
               time_period, time_aggregation,
               season='ANN', mask_ocean=False, overwrite=False,
               region='GLOBAL'):
    """
    Calculate statistics for base and future periods

    Parameters
    ----------
    infile : str
        Full path of the input file. Must contain varn.
    varn : str
        The variable contained in infile.
    scenario: str
        The scenario used, to be able to find matching historical
        files in CMIP6.
    proj: str
        the project the files belong to, either CMIP3, 5, 6
    outpath : str
        Path of the output file. Path must exist.
    time_period : tuple of two strings
        Start and end of the time period. Both strings must be one of
        {"yyyy", "yyyy-mm", "yyyy-mm-dd"}.
    time_aggregation : {'CLIM', 'STD', 'TREND'}
        Type of time aggregation to use.
    season : {'JJA', 'SON', 'DJF', 'MAM', 'ANN'}, optional
    mask_ocean : bool, optional
    overwrite : bool, optional
        If True overwrite existing outfiles otherwise read and return them.
    region : list of strings or str, optional
        Each string must be a valid SREX region

    Returns
    -------
    diagnostic : xarray.DataArray
    """
    def get_outfile(**kwargs):
        kwargs['infile'] = os.path.basename(kwargs['infile']).replace('.nc', '')

        return os.path.join(outpath, '_'.join([
            '{infile}_{time_period[0]}-{time_period[1]}_{season}',
            '{time_aggregation}.nc']).format(**kwargs))

    outfile = get_outfile(infile=infile, time_period=time_period,
                          time_aggregation=time_aggregation, season=season)

    if not overwrite and outfile is not None and os.path.isfile(outfile):
        logger.debug('Diagnostic already exists & overwrite=False, skipping.')
        return xr.open_dataset(outfile)

    if ((proj == 'CMIP5') or (proj == 'CMIP3')):
        da = xr.open_dataset(infile)[varn]
    elif (proj == 'CMIP6'):
        # find historical files matching ssp
        histfile = infile.replace(scenario, 'historical')
        # some ssp runs have not matching historical, continue
        try:
            da_hist = xr.open_dataset(histfile)[varn]
            da_scen = xr.open_dataset(infile)[varn]
        except FileNotFoundError:
            # remove ssp run from model_ensembles
            return
        # concat historical and ssp,
        da = xr.concat([da_hist, da_scen], dim = 'time')
    else:
        logger.error('Wrong project name in config, must be CMIP 3, 5 or 6')

    enc = da.encoding
    assert ((-90 <= da['lat'].data) & (da['lat'].data <= 90)).all(), 'lat has to be in [-90, 90]!'
    errmsg = 'lon has to be in [-180, 180] or [0, 360]!'
    assert (((-180 <= da['lon'].data) & (da['lon'].data <= 180)).all() or
            ((0 <= da['lon'].data) & (da['lon'].data <= 360)).all()), errmsg

    if time_period is not None:
        da = da.sel(time=slice(str(time_period[0]), str(time_period[1])))

    if season in ['JJA', 'SON', 'DJF', 'MAM']:
        da = da.isel(time=da['time.season'] == season)
    elif season is None or season == 'ANN':
        pass
    else:
        raise NotImplementedError('season={}'.format(season))
    if region != 'GLOBAL':
        if (isinstance(region, str) and
            region not in regionmask.defined_regions.srex.abbrevs):
            # if region is not a SREX region read coordinate file
            regionfile = '{}.txt'.format(os.path.join(REGION_DIR, region))
            if not os.path.isfile(regionfile):
                raise ValueError(f'{regionfile} is not a valid regionfile')
            mask = np.loadtxt(regionfile)
            if mask.shape != (4, 2):
                errmsg = ' '.join([
                    f'Wrong file content for regionfile {regionfile}! Should',
                    'contain four lines with corners like: lon, lat'])
                raise ValueError(errmsg)
            lonmin, latmin = mask.min(axis=0)
            lonmax, latmax = mask.max(axis=0)
            if lonmax > 180 or lonmin < -180 or latmax > 90 or latmin < -90:
                raise ValueError(f'Wrong lat/lon value in {regionfile}')
            da = da.salem.roi(corners=((lonmin, latmin), (lonmax, latmax)))
        else:
            if isinstance(region, str):
                region = [region]
            masks = []
            keys = regionmask.defined_regions.srex.map_keys(region)
            for key in keys:
                masks.append(
                    regionmask.defined_regions.srex.mask(da) == key)
            mask = sum(masks) == 1
            da = da.where(mask)

        # drop lats/lons with only nan (=region of interest (ROI))
        da = da.salem.subset(roi=~np.isnan(da.isel(time=0)), margin=1)

    if mask_ocean:
        sea_mask = regionmask.defined_regions.natural_earth.land_110.mask(da) == 0
        da = da.where(sea_mask)

    attrs = da.attrs

    with warnings.catch_warnings():
        # grid cells outside the selected regions are filled with nan and will
        # prompt warning when .mean & .std are called.
        warnings.filterwarnings('ignore', message='Mean of empty slice')
        warnings.filterwarnings('ignore',
                                message='Degrees of freedom <= 0 for slice')

        if time_aggregation == 'CLIM':
            da = da.groupby('time.year').mean('time')
            da = da.mean('year')
        elif time_aggregation == 'STD':
            da = xr.apply_ufunc(detrend, da,
                                input_core_dims=[['time']],
                                output_core_dims=[['time']],
                                keep_attrs=True)
            da = da.std('time')
        elif time_aggregation == 'YEAR':
            da = da.groupby('time.year').mean('time')
        else:
            NotImplementedError(f'time_aggregation={time_aggregation}')

    ds = da.to_dataset(name=varn)
    ds[varn].attrs = attrs
    ds[varn].encoding = enc

    if outfile is not None:
        ds.to_netcdf(outfile)
    return ds

def read_weights(cfg):
    filename = cfg.weights
    ds = xr.open_dataset(filename)

    files = ds['filename']
    model_ensemble = ds['model_ensemble']
    ds_weights = ds['weights']

    return files.data, model_ensemble.data, ds_weights

def calc_significance(data1, data2, alpha=0.05):
    ''' calculates significance for each model'''
    p_vals = []
    h_grids = []
    for model in list(data1['model_ensemble'].data):
        tstat, p_val = stats.ttest_1samp(data1.sel(model_ensemble=model).data,
                                         popmean=data2, axis=0,
                                         nan_policy='omit')
        # in some cases nans are returned, multipletests returns all nans if
        # any nans in p_val arrays
        mask = np.isfinite(p_val)
        p_val_fdr = np.empty(p_val.shape)
        p_val_fdr.fill(np.nan)
        h_grid = np.empty(p_val.shape)
        h_grid.fill(np.nan)
        h_grid[mask], p_val_fdr[mask], _, _ = multipletests(p_val[mask], alpha=alpha, method='fdr_bh')
        #p_val_fdr[mask] = p_val[mask] # for testing without fdr
        #h_grid[mask] = np.where(p_val[mask] < alpha, 1., 0.) # for testing without fdr
        da_p_val_fdr_2D = xr.DataArray(p_val_fdr, name = 'pval',
                                       coords={'lat': data1['lat'],
                                               'lon': data1['lon']},
                                       dims=('lat', 'lon'))
        da_h_grid = xr.DataArray(h_grid, name = 'sig',
                                 coords={'lat': data1['lat'],
                                         'lon': data1['lon']},
                                dims=('lat', 'lon'))
        p_vals.append(da_p_val_fdr_2D)
        h_grids.append(da_h_grid)

    return (xr.concat(h_grids, dim=data1['model_ensemble']),
            xr.concat(p_vals, dim=data1['model_ensemble']))


def main(args):
    """Call functions"""
    logger.info('main().read_config()')
    cfg = read_config(args)
    print(cfg)

    if cfg.weights:
        files, model_ensembles, model_weights = read_weights(cfg)
    else:
        files, model_ensembles = set_up_filenames(cfg, all_members = cfg.ensembles)

    logger.info('Calculating Stats')
    da_base, da_mean_base, da_std_base = calc_data(
        files, model_ensembles, cfg, cfg.base_start, cfg.base_end)

    da_perturbed, da_mean_perturbed, da_std_perturbed = calc_data(
        files, model_ensembles, cfg, cfg.future_start, cfg.future_end)

    # save variable units for further use
    units = da_base.attrs['units']

    # Calculate change from base to perturbed for all models
    diff_all = np.subtract(da_perturbed.data, da_base.data)
    da_rel_all = xr.DataArray(diff_all,
                              coords={
                                  'model_ensemble': da_base['model_ensemble'],
                                  'year': da_base['year'],
                                  'lat': da_base['lat'],
                                  'lon': da_base['lon']},
                              dims=('model_ensemble', 'year', 'lat', 'lon'))

    # if weights given, multiply each model by its weight
    if cfg.weights:
        assert np.isclose(model_weights.sum(), 1.0), "sum of model weights != 1"
        # calculate weighted values
        da_rel_all_weight = da_rel_all * model_weights

        # Calculate weighted mean change
        da_mean_rel = da_rel_all_weight.mean(axis=1).sum(axis=0)
    else:
        model_weights = xr.DataArray(
            np.ones(len(da_mean_base)),
            coords={'model_ensemble': da_base['model_ensemble']},
            dims=('model_ensemble'))
        da_mean_rel = da_mean_perturbed.mean(axis=0) - da_mean_base.mean(axis=0)


    # calculate significance using t-test and fdr
    # also if weights = True this is done on "raw" data!
    sig, p_val = calc_significance(da_rel_all, 0.)

    if cfg.weights:
        nr_of_model_ens = 1000
        sig_weights, ind_we = as_repeats_3D(sig, model_weights, nr_of_model_ens)
        nrmod_sig = np.apply_along_axis(np.nansum, 0, sig_weights)
        nr_of_model_ens = ind_we - 1
        thres = nr_of_model_ens * 0.2
    else:
        nrmod_sig = np.apply_along_axis(np.nansum, 0, sig.data)
        nr_of_model_ens = len(model_ensembles)
        thres = nr_of_model_ens * 0.2

    hatching = np.where(nrmod_sig < thres, 1., 0.)

    # calculate R for each grid point
    logger.info('Calculating R')
#    start_R_3D = time.time()
#    R = robustness_function_3D(da_mean_base, da_mean_perturbed, da_std_base, da_std_perturbed,
#                               weights=model_weights, max_loops=10, anomalies=True)
#    end_R_3D = time.time()
#    print(f"Time taken by calculating R using 3D function is: {end_R_3D - start_R_3D}")
#    print(R.shape)
#    print(np.max(R))
#    print(np.min(R))
    # 1 loop takes about 20sec to calculate without weighting
    # with weighting approx. XXmin per loop....speed up???
    # neither dask=parallelizd, nor ProcessPoolExecutor, nor numba robustness_function_gufunc was faster than simple apply_ufunc
    start_R_apply_ufunc = time.time()
    R = xr.apply_ufunc(robustness_function,
        da_mean_base, da_mean_perturbed, da_std_base, da_std_perturbed,
        kwargs={'anomalies': True, 'weights': model_weights, 'max_loops': 10},
        input_core_dims=[['model_ensemble'],
                         ['model_ensemble'],
                         ['model_ensemble'],
                         ['model_ensemble']],
        output_core_dims=[[]],
        vectorize=True,
    )
    end_of_R_apply_ufunc = time.time()
    print(f"Time taken by calculating R using apply ufunc is: {end_of_R_apply_ufunc - start_R_apply_ufunc}")
    print(np.max(R))
    print(np.min(R))

    # for weighted cases also calculate R without best and without 2 best Models
    # confidence in future outcome should not be different if we didn't have best Models
#    if cfg.weights:
#        weighted=True
#        # find "best model"
#        bestm = model_weights.isel(model_ensemble=model_weights.argmax('model_ensemble'))
#        print(f"Best model is: {bestm}, removed to recalculate R")
#        #remove best model from weights and means and stds
#        weights_drop = model_weights.drop_sel(model_ensemble=bestm['model_ensemble'].data)
#        mean_base_drop = da_mean_base.drop_sel(model_ensemble=bestm['model_ensemble'].data)
#        mean_perturbed_drop = da_mean_perturbed.drop_sel(model_ensemble=bestm['model_ensemble'].data)
#        std_base_drop = da_std_base.drop_sel(model_ensemble=bestm['model_ensemble'].data)
#        std_perturbed_drop = da_std_perturbed.drop_sel(model_ensemble=bestm['model_ensemble'].data)

#        R_2 = xr.apply_ufunc(robustness_function,
#            mean_base_drop, mean_perturbed_drop, std_base_drop, std_perturbed_drop,
#            kwargs={'anomalies': True, 'weights': weights_drop, 'max_loops': 10, 'weighted': weighted},
#            input_core_dims=[['model_ensemble'],
#                             ['model_ensemble'],
#                             ['model_ensemble'],
#                             ['model_ensemble']],
#            output_core_dims=[[]],
#            vectorize=True,
#        )

#        # find second "best model"
#        bestm2 = weights_drop.isel(model_ensemble=weights_drop.argmax('model_ensemble'))
#        print(f"Second best model is: {bestm2}, removed to recalculate R")
#        #remove best model from weights and means and stds
#        weights_drop2 = weights_drop.drop_sel(model_ensemble=bestm2['model_ensemble'].data)
#        mean_base_drop2 = mean_base_drop.drop_sel(model_ensemble=bestm2['model_ensemble'].data)
#        mean_perturbed_drop2 = mean_perturbed_drop.drop_sel(model_ensemble=bestm2['model_ensemble'].data)
#        std_base_drop2 = std_base_drop.drop_sel(model_ensemble=bestm2['model_ensemble'].data)
#        std_perturbed_drop2 = std_perturbed_drop.drop_sel(model_ensemble=bestm2['model_ensemble'].data)

#        R_3 = xr.apply_ufunc(robustness_function,
#            mean_base_drop2, mean_perturbed_drop2, std_base_drop2, std_perturbed_drop2,
#            kwargs={'anomalies': True, 'weights': weights_drop2, 'max_loops': 10, 'weighted': weighted},
#            input_core_dims=[['model_ensemble'],
#                             ['model_ensemble'],
#                             ['model_ensemble'],
#                             ['model_ensemble']],
#            output_core_dims=[[]],
#            vectorize=True,
#        )

#        R_min1 = np.minimum(R, R_2)
#        R = np.minimum(R_min1, R_3)

    # calculate global mean change
    global_mean_change = area_weighted_mean(da_mean_rel)

    # mask where inconsistent model response
    # (R<0.5 but more than 50% of models significant change)
    thres = nr_of_model_ens * 0.5
    da_mean_rel.data = np.where((R < 0.5) & (nrmod_sig >= thres),
                                np.nan, da_mean_rel.data)

    # convert to percent if percent = True
    if cfg.percent == True:
        # convert change to number in percent
        tmp_dat = da_mean_rel.data
        repl = 100. * tmp_dat / np.where(da_mean_base.data != 0.,
                                         da_mean_base.data, np.nan)
        da_mean_rel.data = 100. * tmp_dat / np.where(
            da_mean_base.mean(axis=0).data != 0.,
            da_mean_base.mean(axis=0).data, np.nan)
        units = '%'

    # Save data to netcdfs
    logger.info('Saving data to netcdf')
    outdir = '%s/%s/netcdf' %(cfg.save_path, cfg.varname)
    os.makedirs(outdir, exist_ok=True)

    outfile = '%s/sig_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    sig.fillna(1e20)
    sig.to_netcdf(path=outfile,
                  encoding={'sig': {'_FillValue': 1e20},
                            'lat': {'_FillValue': False},
                            'lon':  {'_FillValue': False}}, mode='w')

    outfile2 = '%s/pval_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    p_val.fillna(1e20)
    p_val.to_netcdf(path=outfile2,
                    encoding={'pval': {'_FillValue': 1e20},
                              'lat': {'_FillValue': False},
                              'lon':  {'_FillValue': False}}, mode='w')

    outfile3 = '%s/mean_rel_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    da_mean_rel.fillna(1e20)
    ds_mean_rel = da_mean_rel.to_dataset(name=cfg.varname)
    ds_mean_rel = ds_mean_rel.assign(number_of_model_ensembles=nr_of_model_ens)
    global_mean_change.fillna(1e20)
    ds_mean_rel = ds_mean_rel.assign(global_mean_change=global_mean_change)
    ds_mean_rel.to_netcdf(path=outfile3,
                          encoding={cfg.varname: {'_FillValue': 1e20},
                                    'lat': {'_FillValue': False},
                                    'lon':  {'_FillValue': False}},
                          mode='w')

    da_R = xr.DataArray(R, name = 'R', coords={'lat': da_base['lat'],
                                               'lon': da_base['lon']},
                        dims=('lat', 'lon'))
    da_R.fillna(1e20)
    outfile4 = '%s/R_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    da_R.to_netcdf(path=outfile4,
                   encoding={'R': {'_FillValue': 1e20},
                             'lat': {'_FillValue': False},
                             'lon':  {'_FillValue': False}}, mode='w')

    da_nrmod_sig = xr.DataArray(nrmod_sig, name = 'nrmod_sig',
                                coords={'lat': da_base['lat'],
                                        'lon': da_base['lon']},
                                dims=('lat', 'lon'))
    da_nrmod_sig.fillna(1e20)
    outfile5 = '%s/Nrmod_sig_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    da_nrmod_sig.to_netcdf(path=outfile5,
                           encoding={'nrmod_sig': {'_FillValue': 1e20},
                                     'lat': {'_FillValue': False},
                                     'lon':  {'_FillValue': False}}, mode='w')

    outfile6 = '%s/hatch_%s-%s_%s_%s.nc' %(
        outdir, cfg.future_start, cfg.future_end, cfg.scenario, cfg.season)
    da_hatch = xr.DataArray(hatching, name = 'hatching',
                            coords={'lat': da_base['lat'],
                                    'lon': da_base['lon']},
                            dims=('lat', 'lon'))
    da_hatch.fillna(1e20)
    da_hatch.to_netcdf(path=outfile6,
                       encoding={'hatching': {'_FillValue': 1e20},
                              'lat': {'_FillValue': False},
                              'lon':  {'_FillValue': False}}, mode='w')

    # Plot data as maps
    logger.info('Plot data')

    plotatts = {'varname': cfg.varname, 'unit':units, 'season':cfg.season,
                'syear_base': cfg.base_start, 'eyear_base': cfg.base_end,
                'syear_fut': cfg.future_start, 'eyear_fut': cfg.future_end,
                'scenario': cfg.scenario}

    outdir = '%s/%s/plots' %(cfg.save_path, cfg.varname)
    os.makedirs(outdir, exist_ok=True)

    plot_map_stip_hatch(da_mean_rel, robustness=R, significance=hatching,
                        path=outdir, attributes=plotatts)
    logger.info('Finished plotting')

if __name__ == "__main__":
    args = read_args()
    utils.set_logger(level=20)
    main(args)
